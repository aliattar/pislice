\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Abstract}{1}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Introduction}{3}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Motivation}{3}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Context}{3}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Pi Slice explained}{4}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}What is a Pi Slice?}{4}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}The Mathematics behind this all}{4}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Benefit for users}{4}{subsection.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}The Pi Slice App}{5}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Purpose}{5}{subsection.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Screenshots}{5}{subsection.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Blockchain Technology and the ERC20 Contract}{6}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Ethereum}{6}{subsection.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}The Flint programming language}{6}{subsection.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}The People behind Pi Slice}{7}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Founders}{7}{subsection.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Advisors}{7}{subsection.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}The ICO}{8}{section.7}
